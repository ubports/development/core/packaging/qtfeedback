Source: qtfeedback-opensource-src
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Debian UBports Team <team+ubports@tracker.debian.org>,
           Timo Jyrinki <timo@debian.org>,
           Mike Gabriel <sunweaver@debian.org>,
Build-Depends: debhelper-compat (= 12),
               libpulse-dev,
               libqt5gui5,
               pkg-kde-tools,
               qtbase5-dev (>= 5.5.1~),
               qtdeclarative5-dev (>= 5.5.1~),
               qml-module-qtquick2 (>= 5.5.1~),
               qtmultimedia5-dev (>= 5.5.1~),
               qttools5-dev-tools,
               xauth,
               xvfb,
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://qt-project.org/
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt/qtfeedback.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt/qtfeedback/

Package: libqt5feedback5
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Qt Feedback module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains Qt Feedback module.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qml-module-qtfeedback
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt5feedback5, ${misc:Depends}, ${shlibs:Depends}
Breaks: qtdeclarative5-qtfeedback-plugin (<< 5.0~git20130529-0ubuntu6~)
Replaces: qtdeclarative5-qtfeedback-plugin (<< 5.0~git20130529-0ubuntu6~)
Description: Qt 5 Feedback QML module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the Qt Feedback QML module for Qt Declarative.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtfeedback5-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libqt5feedback5 (= ${binary:Version}), qtbase5-dev, ${misc:Depends}
Description: Qt 5 Feedback development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files used for building Qt 5
 applications using Qt Feedback library.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtfeedback5-examples
Architecture: all
Section: libdevel
Pre-Depends: ${misc:Pre-Depends},
Depends: ${misc:Depends},
Description: Qt 5 Feedback examples
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the examples for Qt 5 Feedback.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.

Package: qtfeedback5-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Qt 5 Feedback documentation
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the documentation for the Qt 5 Feedback
 module.
 .
 WARNING: This module is not an official part of Qt 5, but instead a git
 snapshot of an ongoing development. The package is very likely to
 change in a binary incompatible way, and no guarantees are given.
